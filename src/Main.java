import java.util.Scanner;
public class Main {
    public static void main(String args[]) {
        double side, area;
        Scanner op = new Scanner(System.in);

        System.out.print("Enter the side length of an equilateral triangle: ");
        side = op.nextDouble();

        area = (Math.sqrt(3) / 4) * (side * side);

        System.out.println("The area of the equilateral triangle is = " + area);
    }
}
